<?php

namespace App\Http\Controllers\Frontend\Form;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

   
    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function uploadAnggaran(Request $request){
        $file = $request->file('anggaran');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }

    public function uploadProsesbisnis(Request $request){
        $file = $request->file('proses_bisnis');
        $tujuan_upload = 'uploadFile';
        $randno = mt_rand(10, 999);
        $nama_file = $file->getClientOriginalName();
        $file->move($tujuan_upload,$randno.$nama_file);
    }
}
